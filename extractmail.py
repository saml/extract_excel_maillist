#!/usr/bin/env python3
# -*- coding: utf-8 -*
import xlrd
import xlwt
import csv
import sys
from sys import stdout
import argparse

def main():
    argp = argparse.ArgumentParser(description='Extract mail adresses from Excel files')
    argp.add_argument('--input', help='Input file in Excel format (.xls or .xlsx)')
    argp.add_argument('emailcol', metavar='E', type=int, help='Column number for email name')
    argp.add_argument('fnamecol', metavar='F', type=int, help='Column number for first name')
    argp.add_argument('lnamecol', metavar='L', type=int, help='Column number for last name')
    args =  argp.parse_args()
    if not args.input or not args.fnamecol or not args.lnamecol or not args.emailcol:
        argp.print_help()
        sys.exit(0)

    book = xlrd.open_workbook(filename=args.input)
    sheet = book.sheet_by_index(0)

    wrt = csv.writer(stdout, delimiter=',', quotechar='\"')
    for i in range(1, sheet.nrows):
        row = sheet.row(i)
        orow = []
        if len(row) >= args.fnamecol:
            orow.append(clean_name(row[args.fnamecol-1]))
        if len(row) >= args.lnamecol:
            orow.append(clean_name(row[args.lnamecol-1]))
        if len(row) >= args.emailcol:
            orow.append(clean_email(row[args.emailcol-1]))
        wrt.writerow(orow)

def clean_email(email):
    email = email.lower()
    email = email.replace(';', '')
    email = email.replace(' ', '')
    email = email.strip('.')
    return email

def clean_name(name):
    name = ucfirst_allnames(name, ' ')
    name = ucfirst_allnames(name, '-')
    return name

def ucfirst_allnames(text, sep):
    return sep.join([s.title() for s in text.split(sep)])

main()
