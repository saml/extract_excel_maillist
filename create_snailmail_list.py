#!/usr/bin/env python3
# -*- coding: utf-8 -*
import xlrd
import csv
import sys
from sys import stdout
import argparse

def main():
    # Parse flags and stuff
    argp = argparse.ArgumentParser(description='Extract mail adresses from Excel files')
    argp.add_argument('--input', help='Input file in Excel format (.xls or .xlsx)')
    args =  argp.parse_args()
    if not args.input:
        argp.print_help()
        sys.exit(0)

    # Open Excel sheet
    book = xlrd.open_workbook(filename=args.input)
    sheet = book.sheet_by_index(0)

    # Start writing output
    wrt = csv.writer(stdout, delimiter='\t', quotechar='\"', quoting=csv.QUOTE_NONE)
    for i in range(1, sheet.nrows):
        row = sheet.row(i)
        fname = row[1]
        lname = row[0]
        address = row[2]
        postno = row[3]
        city = row[4]
        country = row[5]
        wrt.writerow([
            f(fname) + ' ' + f(lname),
            f(address).replace(' v.', ' väg').replace(' V.', ' väg').replace('V.', 'vägen').replace('v.','vägen').replace('g.','gatan').replace('G.', 'gatan'),
            f(postno),
            f(city),
            f(country)])

def f(name):
    if type(name) is not str:
        name = name.value
    name = ucfirst_allnames(name, ' ')
    name = ucfirst_allnames(name, '-')
    return name

def ucfirst_allnames(text, sep):
    return sep.join([s.title() for s in text.split(sep)])

main()
