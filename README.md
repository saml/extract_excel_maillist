# Extract mail list from Excel

If you have an Excel file with mail addresses as well as first and last names
in separate columns, you can use this script to extract a CSV list formatted as
"Firstname,Lastname,Mailaddress", by executing the script, and telling in what
columns in the excel files the data is.

## Usage

```bash
./extractmail.py --input somefile.xls 1 2 3 > maillist.csv
```
