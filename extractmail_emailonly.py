#!/usr/bin/env python3
# -*- coding: utf-8 -*
import xlrd
import xlwt
import csv
import sys
from sys import stdout
import argparse

def main():
    argp = argparse.ArgumentParser(description='Extract mail adresses from Excel files')
    argp.add_argument('--input', help='Input file in Excel format (.xls or .xlsx)')
    args =  argp.parse_args()
    if not args.input:
        argp.print_help()
        sys.exit(0)

    book = xlrd.open_workbook(filename=args.input)
    sheet = book.sheet_by_index(0)

    wrt = csv.writer(stdout, delimiter=',', quotechar='\"')
    for i in range(1, sheet.nrows):
        row = sheet.row(i)
        wrt.writerow([clean_email(row[0].value)])

def clean_email(email):
    email = email.lower()
    email = email.replace(';', '')
    email = email.replace(' ', '')
    email = email.strip('.')
    return email

main()
